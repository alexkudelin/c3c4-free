#pragma once
#include <cmath>
#include <vector>
#include <random>
#include <map>
#include <set>
#include <limits>
#include <cstdint>
#include <future>
#include <iostream>
#include <algorithm>
#include <fstream>

namespace utils
{
    template<typename Iter, typename RandomGenerator>
    Iter select_randomly(Iter, Iter, RandomGenerator&);

    template<typename Iter>
    Iter select_randomly(Iter, Iter);

    std::vector<std::pair<std::size_t, std::size_t>> greedy(const std::vector<std::vector<std::int64_t>>&);
    std::vector<std::pair<std::size_t, std::size_t>> argsort(const std::vector<std::vector<std::int64_t>> &);
    std::vector<std::pair<std::size_t, std::size_t>> degree_balanced_greedy(const std::vector<std::vector<std::int64_t>> &);

    std::int64_t girth(const std::vector<std::vector<std::int64_t>> &);
    std::int64_t get_upper_bound_edge_number(const std::size_t&);
    std::int64_t get_minimal_degree(const std::size_t&);
    std::int64_t calc_weight(const std::vector<std::pair<std::size_t, std::size_t>>&, const std::vector<std::vector<std::int64_t>>&);
    std::set<std::size_t> find_neighbours(const std::size_t, const std::vector<std::vector<std::int64_t>>&, std::set<std::size_t>&);

    void print(const std::vector<std::vector<std::int64_t>> &);
    void print(const std::vector<std::pair<std::size_t, std::size_t>> &);
    void print(const std::set<std::size_t> &);

    void write_to_file(const std::string &, const std::vector<std::vector<std::int64_t>> &, const std::vector<std::pair<std::size_t, std::size_t>> &);

    bool has_cycles(const std::size_t&, const std::size_t&, const std::vector<std::vector<std::int64_t>>&);
    bool has_c3(const std::size_t&, const std::size_t&, const std::vector<std::vector<std::int64_t>>&, std::set<std::size_t>&, std::set<std::size_t>&);
    bool has_c4(const std::size_t&, const std::size_t&, const std::vector<std::vector<std::int64_t>>&, const std::set<std::size_t>&, const std::set<std::size_t>&);
}