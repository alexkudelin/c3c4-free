// std
#include <iostream>
#include <functional>
#include <cstdint>
#include <cstdlib>
#include <string>
#include <string_view>
#include <cmath>
#include <vector>
#include <chrono>
#include "include/utils.hpp"

class Scope
{
    public:
    explicit Scope(std::function<void()>&& on_close)
        : m_onClose{std::move(on_close)}
    {
    }
    ~Scope()
    {
        m_onClose();
    }

    private:
    std::function<void()> m_onClose;
};


std::vector<std::string_view> Split(const std::string_view input, const std::string_view delim) {
    std::size_t last_pos = 0, search_pos = 0;
    std::vector<std::string_view> result;

    while ((search_pos = input.find(delim, last_pos)) != std::string::npos) {
        result.emplace_back(input.substr(last_pos, search_pos - last_pos));
        last_pos = search_pos + 1;
    }

    if (last_pos != input.size()) {
        result.emplace_back(input.substr(last_pos, input.size() - 1 - last_pos));
    }

    return result;
}

int main(int argc, char *argv[])
{
    std::vector<std::pair<std::int64_t, std::int64_t>> input;
    std::ifstream input_file;
    std::size_t count = 0;

    try {
        std::cout << "Open file: " << argv[1] << std::endl;
        input_file.open(argv[1]);

        Scope sc([&input_file]() { input_file.close(); });

        if (!input_file.good()) {
            std::cout << "Unable to open file" << std::endl;
            return -1;
        }
        std::cout << "File is opened" << std::endl;

        std::string read_value;
        {
            std::getline(input_file, read_value);
            const auto result = Split(read_value, " ");
            count = std::stoll(result[2].data());
            std::cout << "Count: " << count << std::endl;
        }

        for (std::size_t i = 0; i < count; ++i) {
            std::getline(input_file, read_value);
            const auto result = Split(read_value, "\t");
            input.emplace_back(std::stoll(result[1].data()), std::stoll(result[2].data()));
        }
    }
    catch (std::exception &ex) {
        std::cout << ex.what() << std::endl;
    }

    auto start = std::chrono::steady_clock::now();

    std::vector<std::vector<std::int64_t>> matrix(input.size(), std::vector<std::int64_t>(input.size(), 0));
    for (std::size_t i = 0; i < input.size(); ++i) {
        for (std::size_t j = i + 1; j < input.size(); ++j) {
            matrix[j][i] = matrix[i][j] = std::abs(input[i].first - input[j].first)
                + std::abs(input[i].second - input[j].second);
        }
    }

    // std::cout << utils::girth(matrix) << std::endl;
    // utils::print(matrix);
    // auto edges = utils::greedy(matrix);
    auto edges = utils::degree_balanced_greedy(matrix);
    // utils::print(edges);
    utils::write_to_file("./Result" + std::to_string(count) + ".txt", matrix, edges);

    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed ns:" << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start)).count() << std::endl;

    return 0;
}
