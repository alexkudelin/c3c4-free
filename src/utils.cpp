#include "include/utils.hpp"

namespace utils
{
    template<typename Iter, typename RandomGenerator>
    Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
        std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
        std::advance(start, dis(g));
        return start;
    }

    template<typename Iter>
    Iter select_randomly(Iter start, Iter end) {
        static std::random_device rd;
        static std::mt19937 gen(rd());
        return select_randomly(start, end, gen);
    }

    std::int64_t girth(const std::vector<std::vector<std::int64_t>> &matrix) {
        std::int64_t g = std::numeric_limits<std::int64_t>::max();

        for (std::int64_t v = 0; v < matrix.size(); ++v) {
            std::set<std::int64_t> s;
            std::set<std::int64_t> r = {v};
            std::map<std::int64_t, std::int64_t> p;
            std::map<std::int64_t, std::int64_t> d;

            p.emplace(v, std::numeric_limits<std::int64_t>::max());
            d.emplace(v, 0);

            while (!r.empty()) {
                std::int64_t x = *select_randomly(r.begin(), r.end());
                s.insert(x);
                r.erase(x);

                std::set<std::int64_t> n;

                for (std::size_t i = 0; i < matrix.size(); ++i) {
                    if (std::int64_t(i) != x && matrix[i][x] != 0) {
                        n.emplace(std::int64_t(i));
                    }
                }

                if (std::find(n.begin(), n.end(), p[x]) != n.end()) {
                    n.erase(p[x]);
                }

                for (auto y : n) {
                    if (std::find(s.begin(), s.end(), y) != s.end()) {
                        g = std::min(g, d[x] + d[y] + 1);
                    } else {
                        p[y] = x;
                        d[y] = d[x] + 1;

                        r.insert(y);
                    }
                }
            }
        }

        return g;
    }

    std::vector<std::pair<std::size_t, std::size_t>> argsort(const std::vector<std::vector<std::int64_t>> &matrix) {
        std::vector<std::pair<std::size_t, std::size_t>> idx;

        for (std::size_t i = 0; i < matrix.size(); ++i) {
            for (std::size_t j = i + 1; j < matrix.size(); ++j) {
                idx.emplace_back(i, j);
            }
        }

        sort(idx.begin(), idx.end(), [&matrix](std::pair<std::size_t, std::size_t> l, std::pair<std::size_t, std::size_t> r) {
            return matrix[l.first][l.second] > matrix[r.first][r.second];
        });

        return idx;
    }

    std::set<std::size_t> find_neighbours(const std::size_t current, const std::vector<std::vector<std::int64_t>>& m, std::set<std::size_t>& ignore) {
        std::set<std::size_t> n;

        for (std::size_t j = 0; j < m[current].size(); ++j) {
            if (j != current && m[current][j] != 0 && (std::find(ignore.begin(), ignore.end(), j) == ignore.end())) {
                n.insert(j);
            }
        }

        return n;
    }

    bool has_c3(const std::size_t& i, const std::size_t& j, const std::vector<std::vector<std::int64_t>> &m, std::set<std::size_t>& n_i, std::set<std::size_t>& n_j) {
        std::set<std::size_t> ignore = {j};

        n_i = utils::find_neighbours(i, m, ignore);

        ignore.clear();
        ignore.insert(i);

        n_j = utils::find_neighbours(j, m, ignore);

        std::set<std::size_t> intersect;
        std::set_intersection(
            n_i.begin(), n_i.end(),
            n_j.begin(), n_j.end(),
            std::inserter(intersect, intersect.begin())
        );

        return intersect.size() == 1;
    }

    bool has_c4(const std::size_t& i, const std::size_t& j, const std::vector<std::vector<std::int64_t>> &m, const std::set<std::size_t>& n_i, const std::set<std::size_t>& n_j) {
        bool c4 = false;
        std::set<std::size_t> ignore = {i, j};

        for (auto neighbour_of_i : n_i) {
            auto neighbours_of_neighbour_of_i = utils::find_neighbours(neighbour_of_i, m, ignore);

            std::set<std::size_t> intersect;
            std::set_intersection(
                neighbours_of_neighbour_of_i.begin(), neighbours_of_neighbour_of_i.end(),
                n_j.begin(), n_j.end(),
                std::inserter(intersect, intersect.begin())
            );

            if (intersect.size() != 0) {
                c4 = true;
                break;
            }
        }

        if (c4) {
            return c4;
        }

        for (auto neighbour_of_j : n_j) {
            auto neighbours_of_neighbour_of_j = utils::find_neighbours(neighbour_of_j, m, ignore);

            std::set<std::size_t> intersect;
            std::set_intersection(
                neighbours_of_neighbour_of_j.begin(), neighbours_of_neighbour_of_j.end(),
                n_i.begin(), n_i.end(),
                std::inserter(intersect, intersect.begin())
            );

            if (intersect.size() != 0) {
                c4 = true;
                break;
            }
        }

        return c4;
    }


    bool has_cycles(const std::size_t& i, const std::size_t& j, const std::vector<std::vector<std::int64_t>>& m) {
        std::set<std::size_t> n_i, n_j;

        return utils::has_c3(i, j, m, n_i, n_j) || utils::has_c4(i, j, m, n_i, n_j);
    }

    std::vector<std::pair<std::size_t, std::size_t>> greedy(const std::vector<std::vector<std::int64_t>> &matrix) {
        auto edges = utils::argsort(matrix);
        std::vector<std::vector<std::int64_t>> subgraph_matrix(matrix.size(), std::vector<std::int64_t>(matrix.size(), 0));

        std::vector<std::pair<std::size_t, std::size_t>> result;

        for (auto edge : edges) {
            auto i = edge.first, j = edge.second;

            if (utils::has_cycles(i, j, subgraph_matrix)) {
                continue;
            }

            subgraph_matrix[i][j] = subgraph_matrix[j][i] = matrix[i][j];
            result.emplace_back(i, j);
        }

        return result;
    }

    std::int64_t get_upper_bound_edge_number(const std::size_t& vertices_number) {
        return (vertices_number * std::sqrt(vertices_number - 1))/2;
    }

    std::int64_t get_minimal_degree(const std::size_t& vertices_number) {
        return  utils::get_upper_bound_edge_number(vertices_number) - utils::get_upper_bound_edge_number(vertices_number - 1);
    }

    std::vector<std::pair<std::size_t, std::size_t>> degree_balanced_greedy(const std::vector<std::vector<std::int64_t>> &matrix) {
        auto v = matrix.size();
        auto e = get_upper_bound_edge_number(v);
        auto delta = get_minimal_degree(v);

        // std::cout << "f(v=" << v << ") = " << e << std::endl;
        // std::cout << "delta(v=" << v << ") = " << delta << std::endl;

        std::map<std::size_t, std::int64_t> degrees;

        auto edges = argsort(matrix);
        std::vector<std::vector<std::int64_t>> subgraph_matrix(matrix.size(), std::vector<std::int64_t>(matrix.size(), 0));

        std::vector<std::pair<std::size_t, std::size_t>> res;

        for (auto edge : edges) {
            auto i = edge.first, j = edge.second;

            if (degrees[i] >= delta || degrees[j] >= delta) {
                continue;
            }

            if (utils::has_cycles(i, j, subgraph_matrix)) {
                continue;
            }

            degrees[i]++;
            degrees[j]++;

            subgraph_matrix[i][j] = subgraph_matrix[j][i] = matrix[i][j];
            res.emplace_back(i, j);
        }

        return res;
    }
    
    void print(const std::vector<std::vector<std::int64_t>> &matrix) {
        for (auto row : matrix) {
            for (auto item : row) {
                std::cout << item << " ";
            }
            std::cout << std::endl;
        }

        std::cout << std::endl;
    }

    void print(const std::vector<std::pair<std::size_t, std::size_t>> &edges) {
        for (auto edge : edges) {
            std::cout << edge.first << " " << edge.second << std::endl;
        }

        std::cout << std::endl;
    }

    void print(const std::set<std::size_t> &s) {
        for (auto item : s) {
            std::cout << item << " ";
        }

        std::cout << std::endl;
    }

    std::int64_t calc_weight(const std::vector<std::pair<std::size_t, std::size_t>>& edges, const std::vector<std::vector<std::int64_t>>& matrix) {
        std::int64_t s = 0;

        for (auto edge : edges) {
            s += matrix[edge.first][edge.second];
        }

        return s;
    }

    void write_to_file(const std::string &filename, const std::vector<std::vector<std::int64_t>> &m, const std::vector<std::pair<std::size_t, std::size_t>>& edges) {
        std::ofstream ofs;
        ofs.open (filename, std::ofstream::out | std::ofstream::trunc);

        ofs << "c Вес подграфа = " << utils::calc_weight(edges, m) << std::endl;
        ofs << "p edge " << m.size() << " " << edges.size() << std::endl;

        for (auto edge : edges) {
            ofs << "e " << edge.first + 1 << " " << edge.second + 1 << std::endl;
        }

        ofs.close();
    }
}
